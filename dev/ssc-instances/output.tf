####################
# Instance
###################

output "instance_id" {
  description = "EC2 instance ID"
  value       = "${module.db-instance.id[0]}"
}

output "instance_name" {
  description = "EC2 instance Name"
  value       = "${module.db-instance.tags[0]}"
}
output "instance_public_dns" {
  description = "Public DNS name assigned to the EC2 instance"
  value       = "${module.db-instance.public_ip[0]}"
}

output "ebs_volume_attachment_id" {
  description = "The volume ID"
  value       = "${aws_volume_attachment.this_ec2.volume_id}"
}

output "ebs_volume_attachment_instance_id" {
  description = "The instance ID"
  value       = "${aws_volume_attachment.this_ec2.instance_id}"
}
