#cloud-config
cloud_final_modules:
- [users-groups,always]
users:
  - name: cloud-user
    groups: [ wheel ]
    sudo: [ "ALL=(ALL) NOPASSWD:ALL" ]
    shell: /bin/bash
    ssh-authorized-keys:
    - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDRo8SnRAjzVbTjUqO84Pl3UjedcjXHPmgWslY8VWu56vqPlhw0U/MK8L5YCkpXqlpR3RGygQIVNyWQ0Tws2RbvMsR5wTm8gUNc0+gemGrPqbLkvwiZtwo+JOFRYTauIvkdytyK+q1Os/gep++81HVPavp3al7YxK9MdFh1ykrhn4hFD0oosTZ4H1/+zMbIkDqcXeNTYMZ5G9hHcCLkFxJUk8oxO6vqidJiUP6/N8/zSIRU8kiznNFhSkDMEhFL1z1nlyKkW6xZ/hMBFmQVP47q0v3e701n9tXP8qwjGTlpDLiJ/XVt7innQCp/rNYJ20iME5AcZwShMMtddQrVM1ev oregon-key
packages:
  - epel-release
  - nano
package_update: true
