terraform {
  backend "s3" {
    bucket  = "axi-tf-remote-state"
    region  = "us-west-2"
    key     = "tf-ict-45-dev/instances/terraform.tfstate"
    encrypt = true
    profile = "s3_backend"
    shared_credentials_file = "../profile/aws_profile"
  }
}

