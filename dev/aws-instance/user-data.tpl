#cloud-config
manage_etc_hosts: true
locale: "en_US.UTF-8"
fqdn: ${instance_name}
cloud_final_modules:
- [users-groups,always]
yum_repos:
  saltrepo:
    baseurl: https://repo.saltstack.com/yum/redhat/\$releasever/\$basearch/archive/2018.3.3
    enabled: true
    failovermethod: priority
    gpgcheck: true
    gpgkey: https://repo.saltstack.com/yum/redhat/\$releasever/\$basearch/archive/2018.3.3/SALTSTACK-GPG-KEY.pub
    name: Salt
users:
  - name: cloud-user
    groups: [ wheel ]
    sudo: [ "ALL=(ALL) NOPASSWD:ALL" ]
    shell: /bin/bash
    ssh-authorized-keys:
    - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDRo8SnRAjzVbTjUqO84Pl3UjedcjXHPmgWslY8VWu56vqPlhw0U/MK8L5YCkpXqlpR3RGygQIVNyWQ0Tws2RbvMsR5wTm8gUNc0+gemGrPqbLkvwiZtwo+JOFRYTauIvkdytyK+q1Os/gep++81HVPavp3al7YxK9MdFh1ykrhn4hFD0oosTZ4H1/+zMbIkDqcXeNTYMZ5G9hHcCLkFxJUk8oxO6vqidJiUP6/N8/zSIRU8kiznNFhSkDMEhFL1z1nlyKkW6xZ/hMBFmQVP47q0v3e701n9tXP8qwjGTlpDLiJ/XVt7innQCp/rNYJ20iME5AcZwShMMtddQrVM1ev oregon-key
packages:
  - epel-release
  - nano
  - salt-minion
runcmd:
  - 'sed -i -e "/^#master: salt/s/^.*\$/master: 10.133.0.72/" /etc/salt/minion'
  - 'systemctl enable salt-minion.service'
  - 'systemctl restart salt-minion.service'
