provider "aws" {
  region = "us-west-2"
  profile = "s3_backend"
  shared_credentials_file = "../profile/aws_profile"
}
