
module "db-instance" {
    source                 = "terraform-aws-modules/ec2-instance/aws"
    version                = "1.12.0"
    name                   = "postgresql"
    instance_count         = 1
    ami                    = "ami-3ecc8f46"
    instance_type          = "t2.small"
    key_name               = "oregon-key"
    monitoring             = true
    vpc_security_group_ids = ["sg-0728c41c336f2231c"]
    subnet_id              = "subnet-0905b543f32ee8581"
    associate_public_ip_address = true
    user_data = "${data.template_cloudinit_config.config.rendered}"
    tags = {
      Terraform = "true"
      Environment = "dev"
  }
}

resource "aws_volume_attachment" "this_ec2" {
  device_name = "/dev/sdb"
  volume_id   = "${aws_ebs_volume.this.id}"
  instance_id = "${module.db-instance.id[0]}"
}

resource "aws_ebs_volume" "this" {
  availability_zone = "${module.db-instance.availability_zone[0]}"
  size              = 1
}
