variable "instance_name" {
  type= "string"
  default = "dev-proxy-aavn.aavn.local"
}


module "nginx-instance" {
    source                 = "terraform-aws-modules/ec2-instance/aws"
    version                = "1.12.0"
    name                   = "${var.instance_name}"
    instance_count         = 1
    ami                    = "ami-b63ae0ce"
    instance_type          = "t2.small"
    key_name               = "oregon-key"
    monitoring             = true
    vpc_security_group_ids = ["sg-0728c41c336f2231c"]
    subnet_id              = "subnet-0905b543f32ee8581"
    associate_public_ip_address = true
    user_data = "${data.template_cloudinit_config.config.rendered}"
    tags = {
      Terraform = "true"
      Environment = "dev"
  }
}

